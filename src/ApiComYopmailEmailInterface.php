<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-com-yopmail-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiComYopmail;

use DateTimeInterface;
use PhpExtended\Email\EmailAddressInterface;
use PhpExtended\Email\MailboxListInterface;
use Stringable;

/**
 * ApiComYopmailEmailInterface interface file.
 * 
 * This represents an email as received by yopmail.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 */
interface ApiComYopmailEmailInterface extends Stringable
{
	
	/**
	 * Gets the target address email.
	 * 
	 * @return EmailAddressInterface
	 */
	public function getAddress() : EmailAddressInterface;
	
	/**
	 * Gets the id of this email.
	 * 
	 * @return string
	 */
	public function getId() : string;
	
	/**
	 * Gets the object of this email.
	 * 
	 * @return string
	 */
	public function getObject() : string;
	
	/**
	 * Gets the address of the sender of this email.
	 * 
	 * @return MailboxListInterface
	 */
	public function getFrom() : MailboxListInterface;
	
	/**
	 * Gets the date of reception of this email.
	 * 
	 * @return DateTimeInterface
	 */
	public function getDateReception() : DateTimeInterface;
	
	/**
	 * Gets the full html contents of this email.
	 * 
	 * @return string
	 */
	public function getContent() : string;
	
}

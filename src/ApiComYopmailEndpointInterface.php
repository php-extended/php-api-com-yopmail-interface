<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-com-yopmail-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiComYopmail;

use InvalidArgumentException;
use PhpExtended\Iterator\PagedIteratorInterface;
use PhpExtended\Parser\ParseThrowable;
use Psr\Http\Client\ClientExceptionInterface;
use RuntimeException;
use Stringable;

/**
 * ApiComYopmailEndpointInterface class file.
 * 
 * This class represents the endpoint for all yopmail accounts.
 * 
 * @author Anastaszor
 */
interface ApiComYopmailEndpointInterface extends Stringable
{
	
	/**
	 * Gets the emails at the given page for given user.
	 * 
	 * @param string $username the name of the user (xxx in xxx@yopmail.com)
	 * @param integer $page
	 * @return PagedIteratorInterface<ApiComYopmailEmailMetadataInterface>
	 * @throws RuntimeException
	 * @throws ParseThrowable
	 * @throws InvalidArgumentException
	 * @throws ClientExceptionInterface
	 */
	public function getEmailMetadatas(string $username, int $page = 1) : PagedIteratorInterface;
	
	/**
	 * Gets the email corresponding to the given metadata.
	 * 
	 * @param ApiComYopmailEmailMetadataInterface $metadata
	 * @return ApiComYopmailEmailInterface
	 * @throws RuntimeException
	 * @throws ParseThrowable
	 * @throws InvalidArgumentException
	 * @throws ClientExceptionInterface
	 */
	public function getEmail(ApiComYopmailEmailMetadataInterface $metadata) : ApiComYopmailEmailInterface;
	
}
